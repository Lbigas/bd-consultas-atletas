SET SQL_SAFE_UPDATES = 0;
# Consulta

# Listar Tecnicos
DELIMITER //
CREATE PROCEDURE Lista_Tecnicos()
BEGIN
SELECT *
FROM Tecnico;
END //
DELIMITER ;

# Listar Atletas
DELIMITER //
CREATE PROCEDURE Lista_Atletas()
BEGIN
SELECT A.id_atleta, A.cc, A.primeiro_nome, A.ultimo_nome, A.sexo, A.data_nascimento,
	   M.nome
FROM Atleta A,
	 Modalidade M
WHERE
	 A.modalidade = M.id_modalidade
GROUP BY M.nome, A.id_atleta;
END //
DELIMITER ;


# Lista Contacto de Atleta
DELIMITER //
CREATE PROCEDURE Lista_Contacto_Atleta(IN id INT)
BEGIN
SELECT C.telemovel, C.telefone, C.email, C.rua, C.porta, CP.cod_postal, CP.cidade
FROM Atleta A,
     Contacto C,
	 CodigoPostal CP
WHERE id = A.id_atleta AND A.contacto = C.id_contacto AND C.cod_postal = CP.cod_postal;
END //
DELIMITER ;

# Lista Testes marcados para um atleta
# Doping
DELIMITER //
CREATE PROCEDURE Lista_Agenda_Doping_Atleta(IN id INT)
BEGIN
SELECT
	tecnico,
	data_marcacao
FROM
	Agenda_Doping
WHERE
	atleta = id AND
	resultados IS NULL
ORDER BY data_marcacao;
END //
DELIMITER ;

# Cardio Pulmonar
DELIMITER //
CREATE PROCEDURE Lista_Agenda_CardPulm_Atleta(IN id INT)
BEGIN
SELECT
	tecnico,
	data_marcacao
FROM
	Agenda_CardPulm
WHERE
	atleta = id AND
	resultados IS NULL
ORDER BY data_marcacao;
END //
DELIMITER ;

# Todos os testes marcados
DELIMITER //
CREATE PROCEDURE Lista_Agenda_Atleta(IN id INT)
BEGIN
SELECT
	"Doping" AS Teste,
	tecnico,
    data_marcacao
FROM
	Agenda_Doping ad
WHERE
	atleta = id AND
    resultados IS NULL
UNION
SELECT
	"Cardio_Pulmonar" AS Teste,
    tecnico,
    data_marcacao
FROM
	Agenda_CardPulm
WHERE
	atleta = id AND
    resultados IS NULL
ORDER BY data_marcacao;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE Lista_Agenda_Tecnico(IN id INT)
BEGIN
SELECT
	"Doping" AS Teste,
	atleta,
    data_marcacao
FROM
	Agenda_Doping ad
WHERE
	tecnico = id AND
    resultados IS NULL
UNION
SELECT
	"Cardio_Pulmonar" AS Teste,
    atleta,
    data_marcacao
FROM
	Agenda_CardPulm
WHERE
	tecnico = id AND
    resultados IS NULL
ORDER BY data_marcacao;
END //
DELIMITER ;

# Lista Testes realizados a um atleta
# Doping
DELIMITER //
CREATE PROCEDURE Lista_Realizado_Doping_Atleta(IN id INT)
BEGIN
SELECT
	tecnico,
	data_marcacao
FROM
	Agenda_Doping
WHERE
	atleta = id AND
	resultados IS NOT NULL
ORDER BY data_marcacao;
END //
DELIMITER ;

# Cardio Pulmonar
DELIMITER //
CREATE PROCEDURE Lista_Realizado_Cardio_Atleta(IN id INT)
BEGIN
SELECT
	tecnico,
	data_marcacao
FROM
	Agenda_CardPulm
WHERE
	atleta = id AND
	resultados IS NOT NULL
ORDER BY data_marcacao;
END //
DELIMITER ;

# Todos testes realizados
DELIMITER //
CREATE PROCEDURE Lista_Realizados_Atleta(IN id INT)
BEGIN
SELECT
	"Doping" AS Teste,
	tecnico,
    data_marcacao
FROM
	Agenda_Doping ad
WHERE
	atleta = id AND
    resultados IS NOT NULL
UNION
SELECT
	"Cardio_Pulmonar" AS Teste,
    tecnico,
    data_marcacao
FROM
	Agenda_CardPulm
WHERE
	atleta = id AND
    resultados IS NOT NULL
ORDER BY data_marcacao;
END //
DELIMITER ;

-- CALL Agendar_Doping(8,1,'2019-12-31 12:00');
-- CALL Agendar_Cardio_Pulmonar(9,1,'2019-12-30 12:00');

DELIMITER //
CREATE PROCEDURE Lista_Realizados_Tecnico(IN id INT)
BEGIN
SELECT
	"Doping" AS Teste,
	atleta,
    data_marcacao
FROM
	Agenda_Doping ad
WHERE
	tecnico = id AND
    resultados IS NOT NULL
UNION
SELECT
	"Cardio_Pulmonar" AS Teste,
    atleta,
    data_marcacao
FROM
	Agenda_CardPulm
WHERE
	tecnico = id AND
    resultados IS NOT NULL
ORDER BY data_marcacao;
END //
DELIMITER ;


# Lista Agenda Completa da Clinica
DELIMITER //
CREATE PROCEDURE Lista_Agenda()
BEGIN
SELECT
	"Doping" AS Teste,
    tecnico,
	atleta,
    data_marcacao
FROM
	Agenda_Doping
WHERE
	resultados IS NULL
UNION
SELECT
	"Cardio_Pulmonar" AS Teste,
    tecnico,
    atleta,
    data_marcacao
FROM
	Agenda_CardPulm
WHERE
    resultados IS NULL
ORDER BY data_marcacao;
END //
DELIMITER ;


# Lista Testes realizados
DELIMITER //
CREATE PROCEDURE Lista_Realizado()
BEGIN
SELECT
	"Doping" AS Teste,
    tecnico,
	atleta,
    data_marcacao,
    hora_realizacao
FROM
	Agenda_Doping
WHERE
	resultados IS NOT NULL
UNION
SELECT
	"Cardio_Pulmonar" AS Teste,
    tecnico,
    atleta,
    data_marcacao,
    hora_realizacao
FROM
	Agenda_CardPulm
WHERE
    resultados IS NOT NULL
ORDER BY data_marcacao;
END //
DELIMITER ;

# Resultado dos testes
# Doping
DELIMITER //
CREATE PROCEDURE Resultado_Doping(IN tec INT, IN atl INT, IN dat DATETIME)
BEGIN
SELECT
	d.analgesicos,
    d.diureticos,
    d.esteroides,
    d.estimulantes,
    d.notas
FROM
	Agenda_Doping ad,
    Doping d
WHERE
	tec = ad.tecnico AND
    atl = ad.atleta AND
    dat = ad.data_marcacao AND
    d.id_doping = ad.resultados;
    
END //
DELIMITER ;

# Cardio Pulmonar
DELIMITER //
CREATE PROCEDURE Resultado_Cardio(IN tec INT, IN atl INT, IN dat DATETIME)
BEGIN
SELECT
	c.VO2,
    c.VCO2,
    c.bpm,
    c.notas
FROM
	Agenda_CardPulm ac,
    Cardio_Pulmonar c
WHERE
	tec = ac.tecnico AND
    atl = ac.atleta AND
    dat = ac.data_marcacao AND
    c.id_CardioPulm = ac.resultados;
    
END //
DELIMITER ;

# Resultados de qualquer teste
DELIMITER //
CREATE PROCEDURE Resultado_Teste(IN tec INT, IN atl INT, IN dat DATETIME)
BEGIN
IF EXISTS(
	SELECT
		*
	FROM
		Agenda_Doping
	WHERE
		tec = tecnico AND
        atl = atleta AND
        dat = data_marcacao)
	THEN
		CALL Resultado_Doping(tec,atl,dat);   
	ELSEIF EXISTS(
	SELECT
		*
	FROM
		Agenda_CardPulm
	WHERE
		tec = tecnico AND
        atl = atleta AND
        dat = data_marcacao)
        THEN
			CALL Resultado_Cardio(tec, atl,dat);
END IF;
END //
DELIMITER ;

# Criacao
# Adicionar Modalidade caso nao exista
DELIMITER //
CREATE PROCEDURE Adicionar_Modalidade (IN n VARCHAR(45))
BEGIN
IF NOT EXISTS(SELECT nome FROM Modalidade WHERE nome = n) THEN
	INSERT INTO Modalidade(nome)
    VALUES(n);
END IF;
END //
DELIMITER ;

# Adicionar Codigo Postal caso nao exista
DELIMITER //
CREATE PROCEDURE Adicionar_Codigo_Postal (IN cod VARCHAR(10), IN nome VARCHAR(45))
BEGIN
IF NOT EXISTS(SELECT cod_postal FROM CodigoPostal WHERE cod_postal = cod) THEN
	INSERT INTO CodigoPostal(cod_postal, cidade)
    VALUES(cod, nome);
END IF;
END //
DELIMITER ;

# Criar Lista de contactos
DELIMITER //
CREATE PROCEDURE Criar_Contacto (IN telm VARCHAR(9), IN telf VARCHAR(9), IN email VARCHAR(45),
                                  IN rua VARCHAR(45), IN porta VARCHAR(45), codPost VARCHAR(10))
BEGIN
INSERT INTO Contacto(telemovel, telefone, email, rua, porta, cod_postal)
VALUES(telm, telf, email, rua, porta, codPost);
END //
DELIMITER ;


# Registar Tecnico
DELIMITER //
CREATE PROCEDURE Registar_Tecnico (IN NumC VARCHAR(8), IN pn VARCHAR(45), IN un VARCHAR(45), IN dat DATE)
BEGIN
IF NOT EXISTS(SELECT cc FROM Tecnico WHERE cc = NumC) THEN
	INSERT INTO Tecnico(cc, primeiro_nome, ultimo_nome, data_nascimento)
    VALUES(NumC, pn, un, dat);
END IF;
END //
DELIMITER ;

# Registar Atleta
DELIMITER //
CREATE PROCEDURE Registar_Atleta (IN cc VARCHAR(8), IN prin VARCHAR(45), IN ultn VARCHAR(45),
								   IN dat DATE, IN s CHARACTER, IN modalidade INT,
                                   IN tel VARCHAR(9), IN telf VARCHAR(9), IN mail VARCHAR(45),
                                   IN rua VARCHAR(45), IN porta VARCHAR(45), IN cp VARCHAR(10))
BEGIN
DECLARE cnt INT;
IF NOT EXISTS(SELECT t.cc FROM Atleta t WHERE t.cc = cc) THEN
	CALL Criar_Contacto(tel, telf, mail, rua, porta, cp);
	SET cnt = LAST_INSERT_ID();
	INSERT INTO Atleta(cc, primeiro_nome, ultimo_nome, data_nascimento, sexo, modalidade, contacto)
    VALUES(cc, prin, ultn, dat, s, modalidade, cnt);
END IF;
END //
DELIMITER ;

# Agendar Teste Dopping
DELIMITER //
CREATE PROCEDURE Agendar_Doping(IN tec INT, IN atl INT, IN dataM DATETIME)
BEGIN
IF NOT EXISTS(SELECT data_marcacao
			  FROM Agenda_Doping
              WHERE tec = tecnico AND ABS(TIMESTAMPDIFF(MINUTE, data_marcacao, dataM)) < 30
					OR atl = atleta AND ABS(TIMESTAMPDIFF(MINUTE, data_marcacao, dataM)) < 30)
   AND
   NOT EXISTS(SELECT data_marcacao
			  FROM Agenda_CardPulm
              WHERE tec = tecnico AND ABS(TIMESTAMPDIFF(MINUTE, data_marcacao, dataM)) < 30
					OR atl = atleta AND ABS(TIMESTAMPDIFF(MINUTE, data_marcacao, dataM)) < 30)
	THEN
	INSERT INTO Agenda_Doping(tecnico, atleta, data_marcacao)
		   VALUES (tec, atl, dataM);
END IF;
END //
DELIMITER ;

# Agendar Teste Cardio Pulmonar
DELIMITER //
CREATE PROCEDURE Agendar_Cardio_Pulmonar(IN tec INT, IN atl INT, IN dataM DATETIME)
BEGIN
IF NOT EXISTS(SELECT data_marcacao
			  FROM Agenda_Doping
              WHERE tec = tecnico AND ABS(TIMESTAMPDIFF(MINUTE, data_marcacao, dataM)) < 30
				 OR atl = atleta AND ABS(TIMESTAMPDIFF(MINUTE, data_marcacao, dataM)) < 30)
   AND
   NOT EXISTS(SELECT data_marcacao
			  FROM Agenda_CardPulm
              WHERE tec = tecnico AND ABS(TIMESTAMPDIFF(MINUTE, data_marcacao, dataM)) < 30
				 OR atl = atleta AND ABS(TIMESTAMPDIFF(MINUTE, data_marcacao, dataM)) < 30)
	THEN
	INSERT INTO Agenda_CardPulm(tecnico, atleta, data_marcacao)
		   VALUES (tec, atl, dataM);
END IF;
END //
DELIMITER ;

# Criar resultado de testes
# Doping
DELIMITER //
CREATE PROCEDURE Criar_Resultado_Doping(IN estim BOOL, IN analg BOOL, IN diur BOOL,
								        IN estr BOOL, IN nota TEXT)
BEGIN
INSERT INTO Doping(estimulantes, analgesicos, diureticos, esteroides, notas)
VALUES(estim, analg, diur, estr, nota);
END //
DELIMITER ;

# Cardio Pulmonar
DELIMITER //
CREATE PROCEDURE Criar_Resultado_CardioPulm(IN o2 DECIMAL(5,2), IN co DECIMAL(5,2), IN bm INT(3),
								        IN nota TEXT)
BEGIN
INSERT INTO Cardio_Pulmonar(VO2, VCO2, bpm, notas)
VALUES(o2,co,bm,nota);
END //
DELIMITER ;

# Realizacao de Testes
# Doping
DELIMITER //
CREATE PROCEDURE Realizar_Doping(IN tec INT, IN atl INT, IN marc DATETIME, IN hora TIME,
                                 IN estim BOOL, IN analg BOOL, IN diur BOOL,
								 IN estr BOOL, IN nota TEXT)
BEGIN
DECLARE res INT;
IF EXISTS(SELECT
			  *
		  FROM
		      Agenda_Doping
		  WHERE
			  tecnico = tec AND
              atleta = atl AND
              data_marcacao = marc AND
              resultados IS NULL)
	THEN
		CALL Criar_Resultado_Doping(estim, analg, diur, estr, nota);
		SET res = LAST_INSERT_ID();
		UPDATE
			Agenda_Doping
		SET
			hora_realizacao = hora,
            resultados = res
	    WHERE
			tecnico = tec AND
            atleta = atl AND
            data_marcacao = marc;
END IF;
END //
DELIMITER ;

# Cardio Pulmonar
DELIMITER //
CREATE PROCEDURE Realizar_CardPulm(IN tec INT, IN atl INT, IN marc DATETIME, IN hora TIME,
								   IN o2 DECIMAL(5,2), IN co2 DECIMAL(5,1), IN bpm INT,
								   nota TEXT)
BEGIN
DECLARE res INT;
IF EXISTS(SELECT
			  *
		  FROM
		      Agenda_CardPulm
		  WHERE
			  tecnico = tec AND
              atleta = atl AND
              data_marcacao = marc AND
              resultados IS NULL)
	THEN
		CALL Criar_Resultado_CardioPulm(o2,co2,bpm,nota);
		SET res = LAST_INSERT_ID();
		UPDATE
			Agenda_CardPulm
		SET
			hora_realizacao = hora,
            resultados = res
	    WHERE
			tecnico = tec AND
            atleta = atl AND
            data_marcacao = marc;
END IF;
END //
DELIMITER ;

# Limpeza
# Remover marcacao
DELIMITER //
CREATE PROCEDURE Remover_Marcacao(IN tec INT, IN atl INT, IN marc DATETIME)
BEGIN
DELETE FROM Agenda_Doping
WHERE
	tec = tecnico AND
    atl = atleta AND
    marc = data_marcacao AND
    resultados IS NULL;

DELETE FROM Agenda_CardPulm
WHERE
	tec = tecnico AND
    atl = atleta AND
    marc = data_marcacao AND
    resultados IS NULL;
END //
DELIMITER ;

# Eliminar Agendamentos com mais de um ano que nao foram realizados
DELIMITER //
CREATE PROCEDURE Limpar_Marcacao()
BEGIN
DELETE FROM Agenda_Doping
WHERE
	data_marcacao < date_add(NOW(), interval -1 month);

DELETE FROM Agenda_CardPulm
WHERE
	data_marcacao < date_add(NOW(), interval -1 month);
END //
DELIMITER ;

call Agendar_Cardio_Pulmonar(1,1,'2020-01-15 12:00');

call Lista_Agenda_Atleta(1);