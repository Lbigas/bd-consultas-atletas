INSERT INTO CodigoPostal
    (cod_postal, cidade)
    VALUES
        ('4700-001', 'Braga'),
        ('4700-002', 'Braga'),
        ('4700-003', 'Braga'),
        ('4700-004', 'Braga'),
        ('4700-005', 'Braga'),
        ('4700-006', 'Braga');

INSERT INTO Modalidade
    (nome)
    VALUES
        ('Corrida'),
        ('Maratona'),
        ('Lançamento'),
        ('Salto');

INSERT INTO Contacto
    (telemovel, telefone, email, rua, porta, cod_postal)
    VALUES
        ('960000000', '253000000', 'bruno@email.com', 'Rua da Misericordia', '5', '4700-001'),
        ('960000001', '253000001', 'sara@email.com', 'Rua do sol nascente', '10', '4700-001'),
        ('960000002', '253000002', 'nuno@email.com', 'Rua Capela de Baixo', '5', '4700-002'),
        ('960000003', '253000003', 'goncalo@email.com', 'Rua da Cachada', '65', '4700-002'),
        ('960000004', '253000004', 'bernardo@email.com', 'Rua do Cruzeiro', '15', '4700-003'),
        ('960000005', '253000005', 'eduardo@email.com', 'Rua das Veigas', '68','4700-004'),
        ('960000006', '253000006', 'ana@email.com', 'Rua da Estrada', '98','4700-004'),
        ('960000007', '253000007', 'mario@email.com', 'Rua do Muro', '150', '4700-005'),
        ('960000008', '253000008', 'rita@email.com', 'Rua do Monte', '49', '4700-005'),
        ('960000009', '253000009', 'dinis@email.com', 'Rua da Igreja', '72', '4700-006');

INSERT INTO Atleta
    (cc, primeiro_nome, ultimo_nome, data_nascimento, sexo,
     modalidade, contacto)
    VALUES
        ('83009738', 'Bruno', 'Ribeiro', '1986-03-30' , 'M', 4, 1),
        ('75328339', 'Sara', 'Lopes', '1987-05-18', 'F', 2, 2),
        ('61019076', 'Nuno', 'Macieira', '1990-06-10', 'M', 2, 3),
        ('54985911', 'Gonçalo', 'Silva', '1992-05-18', 'F', 4, 4),
        ('58836556', 'Bernardo', 'Ribeiro', '1992-06-03', 'M', 1, 5),
        ('56871452', 'Eduardo', 'Pereira', '1993-04-28', 'M', 3, 6),
        ('99270063', 'Ana', 'Carvalho', '1995-04-14', 'F', 1, 7),
        ('44589629', 'Mário', 'Portela', '1996-09-25', 'M', 2, 8),
        ('20550598', 'Rita', 'Teixeira', '1997-09-21', 'F', 2, 9),
        ('77557404', 'Dinis', 'Pinto', '1998-06-21', 'M', 3, 10);

     

INSERT INTO Tecnico
    (cc, primeiro_nome, ultimo_nome, data_nascimento)
    VALUES
        ('44139701', 'Filipe', 'Torres', '1977-06-06'),
        ('90651072', 'Tiago', 'Silva', '1962-12-18'),
        ('82340727', 'Leonor', 'Reis', '1981-12-14'),
        ('45849313', 'Pedro', 'Nunes', '1978-11-27'),
        ('64496291', 'Maria', 'Martins', '1967-01-10'),
        ('81975177', 'Daniel', 'Sousa', '1974-04-17'),
        ('40087700', 'Rodrigo', 'Simões', '1987-08-21'),
        ('47493122', 'Sofia', 'Antunes', '1989-11-12'),
        ('12369634', 'Matilde', 'Rocha', '1980-07-02'),
        ('89742013', 'João', 'Azevedo', '1982-05-05');

INSERT INTO Doping
	(estimulantes, analgesicos, diureticos, esteroides, notas)
    VALUES
		(False, False, False, False, ""),
        (False, False, False, False, ""),
        (False, False, False, False, ""),
        (False, True, False, False, ""),
        (False, False, False, False, ""),
        (False, False, False, False, ""),
        (False, False, False, False, ""),
        (False, False, True, False, ""),
        (False, False, False, False, ""),
        (False, False, False, False, "");

INSERT INTO Agenda_Doping
	(tecnico, atleta, data_marcacao, hora_realizacao, resultados)
    VALUES
		(2,1,'2016-06-13 13:00','13:00',1),
        (1,5,'2016-10-20 14:00','14:00',2),
        (2,2,'2017-02-18 16:00','15:00',3),
        (9,3,'2017-03-29 11:00','12:00',4),
        (7,7,'2017-07-31 10:00','11:00',5),
        (1,9,'2017-11-25 17:00','18:00',6),
        (1,8,'2018-01-06 12:00','12:00',7),
        (9,2,'2018-09-26 13:00','13:00',8),
        (8,1,'2019-01-24 16:00','17:00',9),
        (2,6,'2019-04-09 19:00','18:00',10);

INSERT INTO Cardio_Pulmonar
	(VO2, VCO2, bpm, notas)
    VALUES
		(64.1, 77.5, 145, ""),
        (78.5, 68.0, 178, ""),
        (80.5, 52.3, 154, ""),
        (44.3, 56.3, 143, ""),
        (76.2, 70.2, 126, ""),
        (86.8, 80.5, 163, ""),
        (55.9, 57.4, 182, ""),
        (52.4, 55.1, 173, ""),
        (68.3, 66.2, 163, ""),
        (76.5, 70.4, 195, "");

INSERT INTO Agenda_CardPulm
	(tecnico, atleta, data_marcacao, hora_realizacao, resultados)
    VALUES
		(1,2, '2018-05-20 16:00', '17:00', 1),
        (5,1, '2018-12-01 17:00', '17:30', 2),
        (2,2, '2019-01-05 12:00', '12:05', 3),
        (3,9, '2019-02-07 18:00', '18:00', 4),
        (7,7, '2019-07-05 18:00', '19:30', 5),
        (9,1, '2019-09-17 15:00', '15:20', 6),
        (9,1, '2019-11-01 12:00', '12:00', 7),
        (2,9, '2019-11-15 13:00', '13:10', 8),
        (1,8, '2019-12-07 19:00', '20:00', 9),
        (6,2, '2019-12-16 10:00', '10:02', 10);