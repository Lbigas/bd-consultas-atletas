-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Tecnico`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Tecnico` (
  `id_tecnico` INT NOT NULL AUTO_INCREMENT,
  `cc` VARCHAR(8) NOT NULL,
  `primeiro_nome` VARCHAR(45) NOT NULL,
  `ultimo_nome` VARCHAR(45) NOT NULL,
  `data_nascimento` DATE NOT NULL,
  PRIMARY KEY (`id_tecnico`))
ENGINE = InnoDB
AUTO_INCREMENT = 1;


-- -----------------------------------------------------
-- Table `mydb`.`Modalidade`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Modalidade` (
  `id_modalidade` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_modalidade`))
ENGINE = InnoDB
AUTO_INCREMENT = 1;


-- -----------------------------------------------------
-- Table `mydb`.`CodigoPostal`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`CodigoPostal` (
  `cod_postal` VARCHAR(10) NOT NULL,
  `cidade` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`cod_postal`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Contacto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Contacto` (
  `id_contacto` INT NOT NULL AUTO_INCREMENT,
  `telemovel` VARCHAR(9) NULL,
  `telefone` VARCHAR(9) NULL,
  `email` VARCHAR(45) NULL,
  `rua` VARCHAR(45) NULL,
  `porta` VARCHAR(45) NULL,
  `cod_postal` VARCHAR(10) NULL,
  PRIMARY KEY (`id_contacto`),
  INDEX `fk_Contacto_CodigoPostal1_idx` (`cod_postal` ASC),
  CONSTRAINT `fk_Contacto_CodigoPostal1`
    FOREIGN KEY (`cod_postal`)
    REFERENCES `mydb`.`CodigoPostal` (`cod_postal`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1;


-- -----------------------------------------------------
-- Table `mydb`.`Atleta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Atleta` (
  `id_atleta` INT NOT NULL AUTO_INCREMENT,
  `cc` VARCHAR(8) NOT NULL,
  `primeiro_nome` VARCHAR(45) NOT NULL,
  `ultimo_nome` VARCHAR(45) NOT NULL,
  `data_nascimento` DATE NOT NULL,
  `sexo` CHAR(1) NOT NULL,
  `modalidade` INT NOT NULL,
  `contacto` INT NOT NULL,
  PRIMARY KEY (`id_atleta`),
  INDEX `fk_Atleta_Modalidade1_idx` (`modalidade` ASC),
  INDEX `fk_Atleta_Contacto1_idx` (`contacto` ASC),
  CONSTRAINT `fk_Atleta_Modalidade1`
    FOREIGN KEY (`modalidade`)
    REFERENCES `mydb`.`Modalidade` (`id_modalidade`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Atleta_Contacto1`
    FOREIGN KEY (`contacto`)
    REFERENCES `mydb`.`Contacto` (`id_contacto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1;


-- -----------------------------------------------------
-- Table `mydb`.`Doping`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Doping` (
  `id_doping` INT NOT NULL AUTO_INCREMENT,
  `estimulantes` TINYINT(1) NOT NULL,
  `analgesicos` TINYINT(1) NOT NULL,
  `diureticos` TINYINT(1) NOT NULL,
  `esteroides` TINYINT(1) NOT NULL,
  `notas` TEXT NOT NULL,
  PRIMARY KEY (`id_doping`))
ENGINE = InnoDB
AUTO_INCREMENT = 1;


-- -----------------------------------------------------
-- Table `mydb`.`Agenda_Doping`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Agenda_Doping` (
  `tecnico` INT NOT NULL,
  `atleta` INT NOT NULL,
  `data_marcacao` DATETIME NOT NULL,
  `hora_realizacao` TIME NULL,
  `resultados` INT NULL,
  INDEX `fk_Agenda_Doping_Tecnico1_idx` (`tecnico` ASC),
  INDEX `fk_Agenda_Doping_Atleta1_idx` (`atleta` ASC),
  PRIMARY KEY (`tecnico`, `atleta`, `data_marcacao`),
  INDEX `fk_Agenda_Doping_Doping1_idx` (`resultados` ASC),
  CONSTRAINT `fk_Agenda_Doping_Tecnico1`
    FOREIGN KEY (`tecnico`)
    REFERENCES `mydb`.`Tecnico` (`id_tecnico`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Agenda_Doping_Atleta1`
    FOREIGN KEY (`atleta`)
    REFERENCES `mydb`.`Atleta` (`id_atleta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Agenda_Doping_Doping1`
    FOREIGN KEY (`resultados`)
    REFERENCES `mydb`.`Doping` (`id_doping`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Cardio_Pulmonar`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Cardio_Pulmonar` (
  `id_CardioPulm` INT NOT NULL AUTO_INCREMENT,
  `VO2` DECIMAL(5,2) NOT NULL,
  `VCO2` DECIMAL(5,2) NOT NULL,
  `bpm` INT(3) NOT NULL,
  `notas` TEXT NOT NULL,
  PRIMARY KEY (`id_CardioPulm`))
ENGINE = InnoDB
AUTO_INCREMENT = 1;


-- -----------------------------------------------------
-- Table `mydb`.`Agenda_CardPulm`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Agenda_CardPulm` (
  `tecnico` INT NOT NULL,
  `atleta` INT NOT NULL,
  `data_marcacao` DATETIME NOT NULL,
  `hora_realizacao` TIME NULL,
  `resultados` INT NULL,
  PRIMARY KEY (`tecnico`, `atleta`, `data_marcacao`),
  INDEX `fk_Agenda_CardPulm_Atleta1_idx` (`atleta` ASC),
  INDEX `fk_Agenda_CardPulm_Cardio_Pulmonar1_idx` (`resultados` ASC),
  CONSTRAINT `fk_Agenda_CardPulm_Tecnico1`
    FOREIGN KEY (`tecnico`)
    REFERENCES `mydb`.`Tecnico` (`id_tecnico`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Agenda_CardPulm_Atleta1`
    FOREIGN KEY (`atleta`)
    REFERENCES `mydb`.`Atleta` (`id_atleta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Agenda_CardPulm_Cardio_Pulmonar1`
    FOREIGN KEY (`resultados`)
    REFERENCES `mydb`.`Cardio_Pulmonar` (`id_CardioPulm`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
