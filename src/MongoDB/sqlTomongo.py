import mysql.connector
import codecs
import json 


mydb = mysql.connector.connect(
  host="myHost",
  user="myUser",
  passwd="myPassword",
  database="databaseName"
)

def mongoDateTime(data):
    data=str(data)
    data = data.replace(" ", "T")
    return data

cursor = mydb.cursor()

cursor.execute("SELECT * FROM Atleta")
atletas = cursor.fetchall()

cursor.execute("SELECT * FROM Tecnico")
tecnicos = cursor.fetchall()

cursor.execute("SELECT * FROM Modalidade")
modalidades = cursor.fetchall()

cursor.execute("SELECT * FROM Contacto")
contactos = cursor.fetchall()

cursor.execute("SELECT * FROM CodigoPostal")
codpost = cursor.fetchall()

cursor.execute("SELECT * FROM Agenda_Doping ")
agdop = cursor.fetchall()
for i in agdop:
    i = list(i)
    i[2] = mongoDateTime(i[2])
    i = tuple(i)

cursor.execute("SELECT * FROM Agenda_CardPulm")
agcardio = cursor.fetchall()
for i in agcardio:
    i = list(i)
    i[2] = mongoDateTime(i[2])
    i = tuple(i)

cursor.execute("SELECT * FROM Doping")
doping = cursor.fetchall()

cursor.execute("SELECT * FROM Cardio_Pulmonar")
cardio = cursor.fetchall()

def getTecnicoInfo(x):
    for i in tecnicos:
        if i[0] == x:
            ret = {'id_tecnico': i[0], 'cc': i[1],
                   'primeiro_nome': i[2], 'ultimo_nome': i[3],
                   'data_nascimento': mongoDateTime(i[4])}
            break
    return ret

def getContactoAtleta(id_contacto):
    for i in contactos:
        if i[0] == id_contacto:
            contAux = i
            break
    for i in codpost:
        if contAux[6] == i[0]:
            codAux = i
            break

    cont = {"telemovel": contAux[1], "telefone": contAux[2], "email": contAux[3], "rua": contAux[4], "porta": contAux[5], "codigo_postal": codAux[0], "cidade": codAux[1]}
    return cont

def resCardio(x):
    if x == None:
        ret = {}
    else:
        for i in doping:
            if x == i[0]:
                ret = {'VO2': i[1], 'VCO2': i[2], 'bmp': i[3], 'notas': i[4]}
                break
    return(ret)

def getAgendaCardio(id_atleta):
    cardioRet = []
    for i in agcardio:
        if id_atleta == i[1]:
            # print(resCardio(i[4]))
            resultado = resCardio(i[4])
            if i[3] == None:
                drealiz = ""
            else:
                drealiz = str(i[3])
            tecinfo = getTecnicoInfo(i[0])
            cardioRet.append({ "tecnico": tecinfo,"data_marcacao": str(i[2]).replace(' ','T'),"hora_realizacao": drealiz, "resultados": resultado })
    return cardioRet

def resDoping(x):
    if x == None:
        ret = {}
    else:
        for i in doping:
            if x == i[0]:
                ret = {'estimulantes': i[1], 'analgesicos': i[2],
                        'diureticos': i[3], 'esteroides': i[4],
                        'notas': i[5]}
                break
    return(ret)

def getAgendaDoping(id_atleta):
    dopingRet = []
    for i in agdop:
        if id_atleta == i[1]:
            resultado = resDoping(i[4])
            if i[3] == None:
                drealiz = ""
            else:
                drealiz = str(i[3])
            tecinfo = getTecnicoInfo(i[0])
            dopingRet.append({ "tecnico": tecinfo,"data_marcacao": str(i[2]).replace(' ','T'),"hora_realizacao": drealiz, "resultados": resultado })
    return dopingRet


for x in atletas:

    ### Modalidade do Atleta ###
    for i in modalidades:
        if i[0] == x[6]:
            mod = i[1]

    ### Contacto do Atleta###
    cont = getContactoAtleta(x[7])

    ### Testes ###
    # Doping
    dopingjson=getAgendaDoping(x[0])
    # Cardio Pulmonar
    cardiojson=getAgendaCardio(x[0])

    # documento final para o atleta
    doc = { "id_atleta": x[0], "cc": str(x[1]), "primeiro_nome": str(x[2]),
            "ultimo_nome": str(x[3]),
            "data_nascimento": str(x[4]), "sexo": str(x[5]),
            "modalidade": mod, "contacto": cont,
            "testes": {"doping": dopingjson,
                       "cardio_pulmonar": cardiojson}
            }
    json_str = json.dumps(doc, ensure_ascii=False)
    print (json_str)
