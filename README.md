# Base de Dados para registo de exames de atletas

## mySQL

Inclui modelo lógico (mySQL Workbench), modelo físico (script sql), ficheiros para realizar o povoamento inicial da base de dados e alguns procedimentos para a utilizar.

### Modelo Lógico em mySQL Workbench

![alt text](img/log.png "Modelo Lógico")

## MongoDB

Inclui um script em Python 3 para converter a base de dados inicial em mySQL, para o modelo MongoDB

